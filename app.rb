# encoding: UTF-8

# set the working directory


## Set working directory and load gems
require 'pathname'
## Set up the Root object for easy access.
Root ||= Pathname.new(File.dirname(__FILE__)).expand_path
Dir.chdir Root.to_s
require 'bundler'
Bundler.require(:default, ENV['ENV'].to_s.to_sym)

# # Redis scaling
Plezi::Settings.redis_channel_name = 'my_unique_app_channel_name_67feaade1F'
ENV['PL_REDIS_URL'] ||= ENV['REDIS_URL'] || ENV['REDISCLOUD_URL'] || ENV['REDISTOGO_URL'] # || "redis://username:password@my.host:6389"

# The basic app controller, to get you started
class MyController
	# HTTP - home page
	def index
		# render our welcome page template.
		render :welcome
	end

	# Websockets

	# before connection is established
	def pre_connect
		# return false unless params[:id]
		true
	end
	def on_message data
		data = ERB::Util.html_escape data
		print data
		broadcast :print, data		
	end
	def on_open
		print 'Welcome!'
		broadcast :print, "Somebody joind us :-)"
	end
	def on_close
		broadcast :print, "Somebody left us :-("
	end

	protected

	def print data
		response << data
	end
end


# change some of the default settings here.
host templates: Root.join('templates').to_s

# Add your routes and controllers by order of priority.
route '/(:id)', MyController

